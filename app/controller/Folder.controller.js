sap.ui.define([
    'jquery.sap.global',
    'sap/ui/model/Filter',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'sap/ui/core/mvc/Controller'
], function (jQuery, Filter, JSONModel, MessageToast, Controller) {
    "use strict";

    var FolderController = Controller.extend("lamburus.controller.Folder", {
        mainModel: new JSONModel({"workcenters": [], "workcenter": "ff"}),
        onInit: function () {

            var params = jQuery.sap.getUriParameters(window.location.href);
            console.log(params);

            this.getView().setModel(this.mainModel);

        },
        onAfterRendering: function () {

            this.getWorkcenters("8800");

        },
        toVinPage: function (event) {

            var model = this.getView().getModel();
            var folderId = model.getProperty("/workcenter");
            this.getOwnerComponent().getRouter().navTo("vin", {"folderId": folderId});

        },
        getWorkcenters: function (site) {
            if (!site) {
                site = window.site;
            }
            if (!site) {
                return;
            }

            var that = this;

            var transactionName = "GetWorkCentersByUser";

            var transactionCall = site + "/" + "TRANSACTION" + "/" + transactionName;

            var params = {
                "TRANSACTION": transactionCall,
                "site": site,
                "user_id": "VJTZU15",
                "OutputParameter": "JSON"
            };

            var authFunc = function (xhr) {};
            //TODO: only for develop in local: remove in test
            var username, password;
            if (jQuery.sap.getUriParameters().get("localMode") === "true") {
                jQuery.ajax({
                    dataType: "json",
                    url: "model/pwd4proxy.json",
                    success: function (data, response) {
                        username = data.usr;
                        password = data.pwd;
                        params.j_user = username;
                        params.j_password = password;
                    },
                    async: false
                });
                authFunc = function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                };
            }

            try {
                var req = jQuery.ajax({
                    url: "/XMII/Runner",
                    data: params,
                    method: "POST",
                    dataType: "xml",
                    async: true
                });
                req.done(jQuery.proxy(that.workcentersSuccess, that));
                req.fail(jQuery.proxy(that.workcentersError, that));
            } catch (err) {
                jQuery.sap.log.debug(err.stack);
            }

            if ("" === window.site) {
                this.openMessageDialog(that.resourceManager.getResourceBundle().getText("lux.pod.noSiteForUser"));
            }
        },
        workcentersSuccess: function (data, response) {
            sap.ui.core.BusyIndicator.hide();

            var model = this.getView().getModel();
            var jsonArrStr = jQuery(data).find("Row").text();
            var jsonArrTmp = JSON.parse(jsonArrStr); // jshint ignore:line
            var jsonArr = jsonArrTmp;
            model.setProperty("/workcenters", jsonArr);
            //model.setProperty("/workcenterStatusSize", jsonArr.length);
        },
        workcentersError: function (error) {
            sap.ui.core.BusyIndicator.hide();
        },
        handleValueHelp: function (oEvent) {
            var sInputValue = oEvent.getSource().getValue();

            this.inputLineId = oEvent.getSource().getId();
            // create value help dialog
            if (!this._valueHelpDialog) {
                this._valueHelpDialog = sap.ui.xmlfragment(
                        "lamburus.view.FolderDialog",
                        this);
                this.getView().addDependent(this._valueHelpDialog);
            }

            // create a filter for the binding
            var oFilter = new Filter("name",
                    sap.ui.model.FilterOperator.Contains, sInputValue
                    );
            this._valueHelpDialog.getBinding("items").filter([]);

            // open value help dialog filtered by the input value
            this._valueHelpDialog.open(sInputValue);
        },
        _handleValueHelpSearch: function (evt) {
            var sValue = evt.getParameter("value");
            var oFilter = new Filter("name",
                    sap.ui.model.FilterOperator.Contains, sValue
                    );
            evt.getSource().getBinding("items").filter([oFilter]);
        },
        _handleValueHelpClose: function (evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
            var model = this.getView().getModel();
            if (oSelectedItem) {
                var sPath = oSelectedItem.getBindingContext().getPath();
                var objSelected = model.getProperty(sPath);
                model.setProperty("/workcenter", objSelected.handle);
                var productInput = this.getView().byId(this.inputLineId);
                productInput.setValue(oSelectedItem.getTitle());
            }
            evt.getSource().getBinding("items").filter([]);
        }
    });

    return FolderController;

});
