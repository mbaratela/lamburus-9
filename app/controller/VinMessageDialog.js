sap.ui.define([
    'jquery.sap.global',
    'sap/m/MessageToast',
    'sap/m/MessageBox',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/ui/base/Object'
], function (jQuery, MessageToast, MessageBox, Controller, JSONModel, Object) {
    "use strict";

    var VinMessageDialog = Object.extend("lamburus.controller.VinMessageDialog", {
        _dialog: null,
        _model: new JSONModel({email: ""}),
        _getDialog: function () {
            // create dialog lazily
            if (null === this._dialog) {
                // create dialog via fragment factory
                this._dialog = sap.ui.xmlfragment("lamburus.view.VinMessageDialog", this);
            }
            return this._dialog;
        },
        open: function (oView) {

            this.view = oView;

            var oDialog = this._getDialog();

            // connect dialog to view (models, lifecycle)
            oView.addDependent(oDialog);

            oDialog.setModel(this._model);

            // open dialog
            oDialog.open();
        },
        onCancel: function () {

            this._dialog.close();

        },
        onConfirm: function () {
            try {
                sap.ui.core.BusyIndicator.show(0);
                var that = this;
               
            } catch (err) {
                sap.ui.core.BusyIndicator.hide();
                this._dialog.close();
            }
        },
        _success: function (data, response) {
            sap.ui.core.BusyIndicator.hide();
            jQuery.sap.log.debug(data);
            this._dialog.close();

        },
        _error: function (error) {
            sap.ui.core.BusyIndicator.hide();
            jQuery.sap.log.debug(error);
            var errMsg = "Generic Error";
            this.showErrorMessageBox(errMsg);

        },
        showErrorMessageBox: function (msg) {
            var bCompact = !!this.view.$().closest(".sapUiSizeCompact").length;
            MessageBox.error(
                    msg,
                    {styleClass: bCompact ? "sapUiSizeCompact" : ""}
                            );
        }
    });

    return VinMessageDialog;

});
