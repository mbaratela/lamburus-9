sap.ui.define([
    'jquery.sap.global',
    'sap/m/Button',
    'sap/ui/core/Control'
], function (jQuery, Button, Control) {
    "use strict";

    var PodButton = Button.extend("lamburus.component.PodButton", {
        iconObj: null,
        metadata: {
            properties: {
                "height": {
                    type: "sap.ui.core.CSSSize", defaultValue: "15px"
                }
            }
        },
        renderer: function (oRm, oControl) {

            if (this.iconObj === null && oControl.getIcon()) {
                this.iconObj = new sap.ui.core.Icon();
                this.iconObj.setSrc(oControl.getIcon());
            }

            oRm.write("<div");
            oRm.writeControlData(oControl);
            oRm.addStyle("width", oControl.getWidth());
            oRm.addStyle("height", oControl.getHeight());
            oRm.addStyle("line-height", oControl.getHeight());
            oRm.writeStyles();
            oRm.addClass("materialButton");
            oRm.addClass("materialBlue");
            oRm.writeClasses();
            oRm.write(">");

            if (oControl.getText() && "" !== oControl.getText()) {
                oRm.write("<span");
                oRm.addStyle("display", "inline-block");
                oRm.addStyle("vertical-align", "middle");
                oRm.addStyle("line-height", "normal");
                oRm.addStyle("cursor", "default");
                oRm.writeStyles();
                oRm.write(">");
                oRm.write(oControl.getText());
                oRm.write("</span>");
            }

            if (this.iconObj !== null && oControl.getIcon()) {
                oRm.write("<div>");
                oRm.renderControl(this.iconObj);
                oRm.write("</div>");
            }
            
            oRm.write("</div>");
        },
        onAfterRendering: function () {
            var that = this;
            jQuery.sap.byId(this.getId()).on("mouseup", jQuery.proxy(this.handleClick, this));
            jQuery.sap.byId(this.getId()).on("mousedown", function (e) {
                var target = e.target;
                if (that.getId() !== target.id) {
                    target = target.parentElement;
                }
                var rect = target.getBoundingClientRect();
                var ripple = target.querySelector('.materialRipple');
                jQuery(ripple).remove();
                ripple = document.createElement('span');
                ripple.className = 'materialRipple';
                ripple.style.height = ripple.style.width = Math.max(rect.width, rect.height) + 'px';
                target.appendChild(ripple);
                var top = e.pageY - rect.top - ripple.offsetHeight / 2 - document.body.scrollTop;
                var left = e.pageX - rect.left - ripple.offsetWidth / 2 - document.body.scrollLeft;
                ripple.style.top = top + 'px';
                ripple.style.left = left + 'px';

                return false;
            });
        },
        handleClick: function (event) {
            this.fireEvent("press");
        }
    });

    return PodButton;
});
